import java.util.Random;
public class Board{
	
	Tile[][] grid;
	public Board(){
		Random ram= new Random();
		int gridSetting=ram.nextInt(1);
		this.grid=new Tile[5][5];
		for(int row=0; row<grid.length; row++){
			for(int col=0; col<grid[row].length; col++){
				gridSetting=ram.nextInt(2);
				if(gridSetting==0){
					grid[row][col]=Tile.BLANK;
				}else{
					grid[row][col]=Tile.HIDDEN_WALL;
				}
			}
		}
		
	}
	
	//Overwrite toString
	public String toString(){
		String result="";
		for(int row=0; row<grid.length; row++){
			for(int col=0; col<grid[row].length; col++){
				result+=this.grid[row][col].getName()+" ";
			}
			result+="\n";
		}
		return result;
	}
	
	//custom method
	public int placeToken(int row, int col){
		//1.Data Validation
		if(!(row>=0&&row<this.grid.length&&col>=0&&col<this.grid[0].length)){
			return -2;
		}
		
		if(this.grid[row][col]!=Tile.BLANK&&this.grid[row][col]!=Tile.HIDDEN_WALL){
			return -1;
		}
		if(this.grid[row][col]==Tile.HIDDEN_WALL){
			this.grid[row][col]=Tile.WALL;
			return 1;
		}
		if(this.grid[row][col]==Tile.BLANK){
			this.grid[row][col]=Tile.CASTLE;
		}
		return 0;
		
	}
	
	//method for test
	public static int[] randomNumberArr(){
		Random ram= new Random();
		int[] result={ram.nextInt(9),ram.nextInt(9)};
		return result;
	}
	
	
	
	
}