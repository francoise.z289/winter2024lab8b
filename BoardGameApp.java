import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[]args){
		Board bGame=new Board();
		Scanner reader=new Scanner(System.in);
		System.out.println("Hello, Welcome to Board Game!");
		int numCastles=5;
		int turns=0;
		int row=0;
		int col=0;
		int result=0;
		while(numCastles>0&&turns<8){
			System.out.println("Turns: "+turns+"\t number of Castles: "+numCastles+"\n"+bGame);
			
			turns++;
			
			System.out.println("Choosing Place: enter a number for Row \n Hint: this number should between 0 and 5");
			row=reader.nextInt();
			
			System.out.println("Choosing Place: enter a number for Column \n Hint: this number should between 0 and 5");
			col=reader.nextInt();
			result=bGame.placeToken(row,col);
			if(result<0){
				System.out.println("Error: Invalid row or column");
			}
			if(result==1){
				System.out.println("There is a wall here.");
	
			}
			if(result==0){
				System.out.println("You place a castle here");
				numCastles--;
				
			}
		
		}
		System.out.println("Final Board:\t Turns: "+turns+"\t number of Castles: "+numCastles+"\n"+bGame);
		if(numCastles==0){
			System.out.println("You win the game!");
		}else{
			System.out.println("Sorry, you lost the game");
		}
		
	}
}